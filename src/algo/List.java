package algo;

public interface List<T> {
    void append(T elem);
    T getElement(int index);
    void setElement(int index, T elem);
    void removeElement(int index);
    boolean hasElement(T elem);
    int getSize();
    
}